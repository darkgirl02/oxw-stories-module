<?php

OW::getRouter()->addRoute(new OW_Route('stories-viewlist', 'stories/viewlist/:listType/', 'STORIES_CTRL_Stories', 'viewList', array('listType' => array('default' => 'latest'))));

OW::getRouter()->addRoute(new OW_Route('stories-view-tagged-list_st', 'stories/viewlist/tag/', 'STORIES_CTRL_Stories', 'viewTaggedList'));
OW::getRouter()->addRoute(new OW_Route('stories-view-tagged-list', 'stories/viewlist/tag/:tag', 'STORIES_CTRL_Stories', 'viewTaggedList'));

OW::getRouter()->addRoute(new OW_Route('stories-view-author-list_st', 'stories/viewlist/author/', 'STORIES_CTRL_Stories', 'viewAuthorList'));
OW::getRouter()->addRoute(new OW_Route('stories-view-author-list', 'stories/viewlist/author/:author', 'STORIES_CTRL_Stories', 'viewAuthorList'));

OW::getRouter()->addRoute(new OW_Route('stories-submit', 'stories/submit', 'STORIES_CTRL_Submit', 'index'));

OW::getRouter()->addRoute(new OW_Route('stories-view', 'stories/view/:storyID', 'STORIES_CTRL_Stories', 'viewStory'));
OW::getRouter()->addRoute(new OW_Route('stories-edit', 'stories/edit/:storyID', 'STORIES_CTRL_Edit', 'index'));
OW::getRouter()->addRoute(new OW_Route('stories-feature', 'stories/feature/:storyID', 'STORIES_CTRL_Stories', 'featureStory'));
OW::getRouter()->addRoute(new OW_Route('stories-delete', 'stories/delete/:storyID', 'STORIES_CTRL_Stories', 'deleteStory'));


OW::getRouter()->addRoute(new OW_Route('stories-admin', 'admin/plugins/stories', 'STORIES_CTRL_Admin', 'category'));

STORIES_CLASS_EventHandler::getInstance()->init();