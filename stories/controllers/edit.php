<?php

class STORIES_CTRL_Edit extends OW_ActionController
{
	
	private $service;
	private $fileHandler;
	
	public function __construct()
	{
		$this->service = STORIES_BOL_Service::getInstance();
		$this->fileHandler = STORIES_CLASS_NewFileHandler::getInstance();
		
		 if ( !OW::getRequest()->isAjax() )
        {
            OW::getNavigation()->activateMenuItem(OW_Navigation::MAIN, 'stories', 'stories_menu_item');
        }
	}
	
	public function index(array $params)
	{
		if (!isset($params['storyID']))
		{
			throw new Redirect404Exception();
		}
		
		$story = $this->service->getStory($params['storyID']);
		
		if (!$story)
		{
			throw new Redirect404Exception();
		}
		
		$modPermissions = OW::getUser()->isAuthorized('stories');
		$contentOwner = (int) $story->owner;
		$userId = OW::getUser()->getId();
		$ownerMode = $contentOwner == $userId;
		
		if (!$ownerMode && !$modPermissions)
        {
            throw new AuthorizationException();
        }
		
		$this->assign('storyTitle', $story->title);
		$this->assign('moderatorMode', $modPermissions);
		$this->assign('ownerMode', $ownerMode);
		
		
		/* Set the page title. */
		$this->setPageTitle('Edit Story');
		/* Set the page heading. */
		$this->setPageHeading('Edit Story');
		
		/* Fetch the story categories from the database. */
		$storyCategories = array();
		$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
		foreach ($categories as $category)
		{
			$storyCategories[$category->scode] = $category->name;
		}
		
		$form = new Form('edit_story_form');
		$form->setEnctype(Form::ENCTYPE_MULTYPART_FORMDATA);
		$this->addForm($form);
		
		$fieldDescription = new Textarea('story_description');
		$fieldDescription->setLabel($this->text('stories', 'form_label_story_description'));
		$fieldDescription->setDescription($this->text('stories', 'form_desc_story_desc'));
		$fieldDescription->setRequired();
		$fieldDescription->setValue($story->description);
		$form->addElement($fieldDescription);
		
		$fieldCategories = new CheckBoxGroup('categories');
		$fieldCategories->setLabel($this->text('stories', 'form_label_story_categories'));
		$fieldCategories->setDescription($this->text('stories', 'form_desc_story_categories'));
		$fieldCategories->setRequired();
		$fieldCategories->setOptions($storyCategories);
		$fieldCategories->setColumnCount(3);
		$fieldCategories->setValue($story->getTags());
		$form->addElement($fieldCategories);
		
		if ($modPermissions)
		{
			$fieldMainFile1 = new FileField('story_file_html');
			$fieldMainFile1->setLabel($this->text('stories', 'form_label_story_main_file'));
			$fieldMainFile1->setDescription('Story file as HTML');
			$form->addElement($fieldMainFile1);
		}
		
		$submit = new Submit('submitStoryEdit');
		$submit->setValue('Update Story');
		$form->addElement($submit);
		
		if (OW::getRequest()->isPost() && $form->isValid($_POST))
		{
			
			$data = $form->getValues();
			
			$description = nl2br(htmlspecialchars($data['story_description']));
			$categories = $data['categories'];
			
			$story->description = $description;
			$story->setTags($categories);
			
			if ($modPermissions)
			{
				if (is_uploaded_file($_FILES['story_file_html']['tmp_name']))
				{
					$storyHtmlFile = $this->fileHandler->getStoryHtmlFile($story->getAuthorName(), $story->title, $story->randomNum);
					move_uploaded_file($_FILES['story_file_html']['tmp_name'], $storyHtmlFile);
				}
			}
			
			$this->service->updateStory($story);
			OW::getFeedback()->info('Story Updated');
			$this->redirect(OW::getRouter()->urlForRoute('stories-view', array('storyID' => $story->id)));
		}
		
	}
	
	/** Utility function to load text from the language */
	private function text($prefix, $key, array $vars = null)
	{
		return OW::getLanguage()->text($prefix, $key, $vars);
	}
}