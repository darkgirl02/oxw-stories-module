<?php

class STORIES_CTRL_Submit extends OW_ActionController
{
	
	private $service;
	private $fileHandler;
	
	public function __construct()
	{
		$this->service = STORIES_BOL_Service::getInstance();
		$this->fileHandler = STORIES_CLASS_NewFileHandler::getInstance();
		
		 if ( !OW::getRequest()->isAjax() )
        {
            OW::getNavigation()->activateMenuItem(OW_Navigation::MAIN, 'stories', 'stories_menu_item');
        }
	}
	
	/* Called when the submit story route is invoked. */
	public function index()
	{
		/* Is moderator */
		$modPermissions = OW::getUser()->isAuthorized('stories');
		if ( !OW::getUser()->isAuthorized('stories', 'add') && !$modPermissions )
        {
            $error = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'add');
            throw new AuthorizationException($error['msg']);
        }
		/* Set the page title. */
		$this->setPageTitle($this->text('stories', 'submit_page_title'));
		/* Set the page heading. */
		$this->setPageHeading($this->text('stories', 'submit_story_heading'));
		
		/* Fetch the story categories from the database. */
		$storyCategories = array();
		$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
		foreach ($categories as $category)
		{
			$storyCategories[$category->scode] = $category->name;
		}
		
		/* Fetch some profile data for the user. */
		$questionData = BOL_QuestionService::getInstance()->getQuestionData(array(OW::getUser()->getId()), array('username'));
		
		/* Create the form */
		$form = new Form('submit_story_form');
		$form->setEnctype(Form::ENCTYPE_MULTYPART_FORMDATA);
		/* Add the form */
		$this->addForm($form);
		
		/* Create and setup the story title field */
		$fieldTitle = new TextField('story_title');
		$fieldTitle->setLabel($this->text('stories', 'form_label_story_title'));
		$fieldTitle->setDescription($this->text('stories', 'form_desc_story_title'));
		$fieldTitle->setRequired();
		$form->addElement($fieldTitle);
		
		/* Create and setup the story description field */
		$fieldDescription = new Textarea('story_description');
		$fieldDescription->setLabel($this->text('stories', 'form_label_story_description'));
		$fieldDescription->setDescription($this->text('stories', 'form_desc_story_desc'));
		$fieldDescription->setRequired();
		$form->addElement($fieldDescription);
		
		/* Create and setup the authors name field */
		$fieldAuthorsName = new TextField('author_name');
		$fieldAuthorsName->setLabel($this->text('stories', 'form_label_author_name'));
		$fieldAuthorsName->setDescription($this->text('stories', 'form_desc_author_name'));
		$fieldAuthorsName->setRequired();
		$fieldAuthorsName->setValue($questionData[OW::getUser()->getId()]['username']); // Set the authors name by default to the current users name
		$form->addElement($fieldAuthorsName);
		
		/* Create and setup the checkboxes for story categoreis */
		$fieldCategories = new CheckBoxGroup('categories');
		$fieldCategories->setLabel($this->text('stories', 'form_label_story_categories'));
		$fieldCategories->setDescription($this->text('stories', 'form_desc_story_categories'));
		$fieldCategories->setRequired();
		$fieldCategories->setOptions($storyCategories);
		$fieldCategories->setColumnCount(3);
		$form->addElement($fieldCategories);
		
		/* Create and setup the file field for the main file */
		$fieldMainFile = new FileField('story_file');
		$fieldMainFile->setLabel($this->text('stories', 'form_label_story_main_file'));
		$fieldMainFile->setDescription($this->text('stories', 'form_desc_story_mainfile'));
		$form->addElement($fieldMainFile);
		
		
		$fieldDocxFile = new FileField('story_file_docx');
		$fieldDocxFile->setLabel('MS Word File');
		$fieldDocxFile->setDescription('An optional .docx file the reader can download');
		$form->addElement($fieldDocxFile);
		
		
		$fieldOdtFile = new FileField('story_file_odt');
		$fieldOdtFile->setLabel('Open Document Format');
		$fieldOdtFile->setDescription('An optional .otd file the reader can download');
		$form->addElement($fieldOdtFile);
		
		$fieldRtfFile = new FileField('story_file_rtf');
		$fieldRtfFile->setLabel('Rich Text File');
		$fieldRtfFile->setDescription('An optional .rtf file the reader can download');
		$form->addElement($fieldRtfFile);
		
		$fieldPdfFile = new FileField('story_file_pdf');
		$fieldPdfFile->setLabel('PDF File');
		$fieldPdfFile->setDescription('An optional .pdf file the reader can download');
		$form->addElement($fieldPdfFile);
		
		$fieldEpubFile = new FileField('story_file_epub');
		$fieldEpubFile->setLabel('Ebook');
		$fieldEpubFile->setDescription('An optional .epub file the reader can download');
		$form->addElement($fieldEpubFile);
		
		
		/* Create and setup the submit button */
		$submit = new Submit('submitStory');
		$submit->setValue($this->text('stories', 'form_label_submit'));
		$form->addElement($submit);
		
		if (OW::getRequest()->isPost() && $form->isValid($_POST))
		{
			
			if (!is_uploaded_file($_FILES['story_file']['tmp_name']))
			{
				return;
			}
			
			$filename = $_FILES['story_file']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if ($ext !== 'txt')
			{
				return;
			}
			
			/* Get the form data */
			$data = $form->getValues();
			
			$authorName = $data['author_name'];
			$storyTitle = $data['story_title'];
			$description = UTIL_HtmlTag::stripJs($data['story_description']);
			$random = rand();
			
			$storyFile = $this->fileHandler->getStoryFile($authorName, $storyTitle, $random);
			
			/* move the uplaoded file into it's directory */
			move_uploaded_file($_FILES['story_file']['tmp_name'], $storyFile);
			
			// TODO: Move the other files into correct dir.	
			if (is_uploaded_file($_FILES['story_file_docx']['tmp_name']))
			{
				$docxFile = $this->fileHandler->getStoryExtraFile($authorName, $storyTitle, $random, '.docx');
				move_uploaded_file($_FILES['story_file_docx']['tmp_name'], $docxFile);
			}
			
			if (is_uploaded_file($_FILES['story_file_odt']['tmp_name']))
			{
				$docxFile = $this->fileHandler->getStoryExtraFile($authorName, $storyTitle, $random, '.odt');
				move_uploaded_file($_FILES['story_file_odt']['tmp_name'], $docxFile);
			}
			
			if (is_uploaded_file($_FILES['story_file_rtf']['tmp_name']))
			{
				$rtfFile = $this->fileHandler->getStoryExtraFile($authorName, $storyTitle, $random, '.rtf');
				move_uploaded_file($_FILES['story_file_rtf']['tmp_name'], $rtfFile);
			}
			
			if (is_uploaded_file($_FILES['story_file_pdf']['tmp_name']))
			{
				$pdfFile = $this->fileHandler->getStoryExtraFile($authorName, $storyTitle, $random, '.pdf');
				move_uploaded_file($_FILES['story_file_pdf']['tmp_name'], $pdfFile);
			}
			
			if (is_uploaded_file($_FILES['story_file_epub']['tmp_name']))
			{
				$epubFile = $this->fileHandler->getStoryExtraFile($authorName, $storyTitle, $random, '.epub');
				move_uploaded_file($_FILES['story_file_epub']['tmp_name'], $epubFile);
			}
			
			
			$id = STORIES_BOL_Service::getInstance()->addStory(
				htmlspecialchars($storyTitle), 
				nl2br(htmlspecialchars($description)), 
				$data['categories'], 
				htmlspecialchars($authorName),
				OW::getUser()->getId(),
				time(),
				$random);
			
			$eparams = array(
				'userId' => OW::getUser()->getId(),
				'title' => htmlspecialchars($data['story_title']),
				'storyId' => $id
			);
			
			$event = new OW_Event(STORIES_CLASS_EventHandler::EVENT_STORY_ADD, $eparams);
			OW::getEventManager()->trigger($event);
			
			$this->redirect(OW::getRouter()->urlForRoute('stories-view', array('storyID' => $id)));
		}
	}
	
	/** Utility function to load text from the language */
	private function text($prefix, $key, array $vars = null)
	{
		return OW::getLanguage()->text($prefix, $key, $vars);
	}
	
}