<?php

class STORIES_CTRL_Stories extends OW_ActionController
{
	/**
	 * @var STORIES_BOL_Service
	 */
	private $service;
	private $fileHandler;
	private $menu;
	
	public function __construct()
	{
		$this->service = STORIES_BOL_Service::getInstance();
		$this->fileHandler = STORIES_CLASS_NewFileHandler::getInstance();
		$this->menu = $this->getMenu();
		
		 if ( !OW::getRequest()->isAjax() )
        {
            OW::getNavigation()->activateMenuItem(OW_Navigation::MAIN, 'stories', 'stories_menu_item');
        }
	}
	
	private function getMenu()
	{
		$validLists = array('all', 'latest', 'featured', 'toprated', 'tag', 'author');
		$classes = array('ow_ic_lense', 'ow_ic_clock', 'ow_ic_push_pin', 'ow_ic_star', 'ow_ic_tag', 'ow_ic_user');
		
		$menuItems = array();
		
		$itemZ = new BASE_MenuItem();
		$itemZ->setLabel('All');
		$itemZ->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'all')));
		$itemZ->setKey('all');
		$itemZ->setIconClass($classes[0]);
		$itemZ->setOrder(0);
		array_push($menuItems, $itemZ);
		
		$itemOne = new BASE_MenuItem();
		$itemOne->setLabel('Latest');
		$itemOne->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'latest')));
		$itemOne->setKey('latest');
		$itemOne->setIconClass($classes[1]);
		$itemOne->setOrder(1);
		array_push($menuItems, $itemOne);		
	
		$itemOne = new BASE_MenuItem();
		$itemOne->setLabel('Featured');
		$itemOne->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'featured')));
		$itemOne->setKey('featured');
		$itemOne->setIconClass($classes[2]);
		$itemOne->setOrder(2);
		array_push($menuItems, $itemOne);
		
/*		$itemOne = new BASE_MenuItem();
		$itemOne->setLabel('Top Rated');
		$itemOne->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'toprated')));
		$itemOne->setKey('toprated');
		$itemOne->setIconClass($classes[3]);
		$itemOne->setOrder(3);
		array_push($menuItems, $itemOne); */
		
		$itemOne = new BASE_MenuItem();
		$itemOne->setLabel('By Tag');
		$itemOne->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'tag')));
		$itemOne->setKey('tag');
		$itemOne->setIconClass($classes[4]);
		$itemOne->setOrder(4);
		array_push($menuItems, $itemOne);
		
		$itemOne = new BASE_MenuItem();
		$itemOne->setLabel('By Author');
		$itemOne->setUrl(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'author')));
		$itemOne->setKey('author');
		$itemOne->setIconClass($classes[5]);
		$itemOne->setOrder(5);
		array_push($menuItems, $itemOne);
	
		$menu = new BASE_CMP_ContentMenu($menuItems);
	
		return $menu;
		
	}
	
	public function viewList(array $params)
	{
		$listType = isset($params['listType']) ? $params['listType'] : 'latest';
		
		$validLists = array('all', 'latest', 'featured', 'toprated', 'author', 'tag');
		
		if (!in_array($listType, $validLists))
		{
			$this->redirect(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'latest')));
		}
		
		/* Is moderator */
		$modPermissions = OW::getUser()->isAuthorized('stories');
		if (!OW::getUser()->isAuthorized('stories', 'view') && !$modPermissions)
		{
			$error = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'view');
            throw new AuthorizationException($error['msg']);
		}
		
		$status = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'add');
		
		if ( $status['status'] == BOL_AuthorizationService::STATUS_AVAILABLE )
		{
			$showAddButton = true;
		}
		else if ( $status['status'] == BOL_AuthorizationService::STATUS_PROMOTED )
		{
			$showAddButton = true;
		}
		else
		{
			$showAddButton = false;
		}
		
		$this->assign('showAddButton', $showAddButton);
		
		$this->addComponent('storiesMenu', $this->menu);
		
		$this->assign('listType', $listType);
		
		OW::getDocument()->setHeading('Browse Stories');
		
	}
	
	public function viewTaggedList(array $params = null)
	{
		$tag = !empty($params['tag']) ? trim(htmlspecialchars(urldecode($params['tag']))) : '';
		
		$this->addComponent('storiesMenu', $this->menu);
		$this->menu->getElement('tag')->setActive(true);
		
		OW::getDocument()->setHeading('Browse Stories');
		
		/* Is moderator */
		$modPermissions = OW::getUser()->isAuthorized('stories');
		if (!OW::getUser()->isAuthorized('stories', 'view') && !$modPermissions)
		{
			$error = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'view');
            throw new AuthorizationException($error['msg']);
		}
		
		$status = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'add');
		
		if ( $status['status'] == BOL_AuthorizationService::STATUS_AVAILABLE )
		{
			$showAddButton = true;
		}
		else if ( $status['status'] == BOL_AuthorizationService::STATUS_PROMOTED )
		{
			$showAddButton = true;
		}
		else
		{
			$showAddButton = false;
		}
		
		$this->assign('showAddButton', $showAddButton);
		
		if (strlen($tag))
		{
			$this->assign('tag', $tag);
			$this->assign('listType', 'tag');
		}
		else
		{			
			$storyCategories = array();
			$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
			foreach ($categories as $category)
			{
				$storyCategories[$category->id]['scode'] = $category->scode;
				$storyCategories[$category->id]['name'] = $category->name;
				$storyCategories[$category->id]['description'] = $category->description;
			}
			$this->assign('tagList', $storyCategories);
		}
	}
	
	public function viewAuthorList(array $params = null)
	{
		$author = !empty($params['author']) ? trim(htmlspecialchars(urldecode($params['author']))) : '';
		$this->addComponent('storiesMenu', $this->menu);
		$this->menu->getElement('author')->setActive(true);
		
		OW::getDocument()->setHeading('Browse Stories');
		
		/* Is moderator */
		$modPermissions = OW::getUser()->isAuthorized('stories');
		if (!OW::getUser()->isAuthorized('stories', 'view') && !$modPermissions)
		{
			$error = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'view');
            throw new AuthorizationException($error['msg']);
		}
		
		$status = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'add');
		
		if ( $status['status'] == BOL_AuthorizationService::STATUS_AVAILABLE )
		{
			$showAddButton = true;
		}
		else if ( $status['status'] == BOL_AuthorizationService::STATUS_PROMOTED )
		{
			$showAddButton = true;
		}
		else
		{
			$showAddButton = false;
		}
		
		$this->assign('showAddButton', $showAddButton);
		
		if (strlen($author))
		{
			$this->assign('author', $author);
			$this->assign('listType', 'author');
		}
		else
		{
			$storyAuthors = array();
			$authors = STORIES_BOL_Service::getInstance()->getAuthorList();
			foreach ($authors as $aut)
			{
				$storyAuthors[$aut->id]['id'] = $aut->id;
				$storyAuthors[$aut->id]['name'] = $aut->name;
			}
			$this->assign('authorList', $storyAuthors);
		}
		
	}
	
	/** View the story with the given ID */
	public function viewStory(array $params)
	{
		if (!isset($params['storyID']))
		{
			throw new Redirect404Exception();
		}
		
		$story = $this->service->getStory($params['storyID']);
		
		if (!$story)
		{
			throw new Redirect404Exception();
		}
		
		$userId = OW::getUser()->getId();
		$contentOwner = (int) $story->owner;
		$ownerMode = $contentOwner == $userId;
		
		$modPermissions = OW::getUser()->isAuthorized('stories');
		
		if ( !OW::getUser()->isAuthorized('stories', 'view') && !$modPermissions )
        {
            $error = BOL_AuthorizationService::getInstance()->getActionStatus('stories', 'view');
            throw new AuthorizationException($error['msg']);
        }
		
		$title = $story->title;
		$description = $story->description;
		$authorName = $story->getAuthorName();
		$ownerName = $story->getOwnerName();
		$datePubblished = $story->publicationDate;
		
		$this->assign('storyId', $params['storyID']);
		$this->assign('storyTitle', $title);
		$this->assign('storyDescription', $description);
		$this->assign('authorName', $authorName);
		$this->assign('ownerName', $ownerName);
		$this->assign('datePubblished', $datePubblished);
		$this->assign('moderatorMode', $modPermissions);
		$this->assign('ownerMode', $ownerMode);
		$this->assign('authorId', $story->getAuthor()->id);
		
		$storyCategories = array();
		$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
		foreach ($categories as $category)
		{
			$storyCategories[$category->scode]['scode'] = $category->scode;
			$storyCategories[$category->scode]['name'] = $category->name;
		}
		$tags = array();
		foreach ($story->getTags() as $tag)
		{
			$tags[$tag] = $storyCategories[$tag];
		}
		$this->assign('tags', $tags);
		
		$hasDownload = false;
		
		$docxFile = $this->fileHandler->getStoryExtraFile($story->getAuthorName(), $story->title, $story->randomNum, '.docx');
		if (file_exists($docxFile))
		{
			$url = $this->fileHandler->getStoryExtraFileUrl($story->getAuthorName(), $story->title, $story->randomNum, '.docx');
			$hasDownload = true;
			$this->assign('docxFile', $url);
		}
		
		$odtFile = $this->fileHandler->getStoryExtraFile($story->getAuthorName(), $story->title, $story->randomNum, '.odt');
		if (file_exists($odtFile))
		{
			$url = $this->fileHandler->getStoryExtraFileUrl($story->getAuthorName(), $story->title, $story->randomNum, '.odt');
			$hasDownload = true;
			$this->assign('odtFile', $url);
		}
		
		$rtfFile = $this->fileHandler->getStoryExtraFile($story->getAuthorName(), $story->title, $story->randomNum, '.rtf');
		if (file_exists($rtfFile))
		{
			$url = $this->fileHandler->getStoryExtraFileUrl($story->getAuthorName(), $story->title, $story->randomNum, '.rtf');
			$hasDownload = true;
			$this->assign('rtfFile', $url);
		}
		
		$pdfFile = $this->fileHandler->getStoryExtraFile($story->getAuthorName(), $story->title, $story->randomNum, '.pdf');
		if (file_exists($pdfFile))
		{
			$url = $this->fileHandler->getStoryExtraFileUrl($story->getAuthorName(), $story->title, $story->randomNum, '.pdf');
			$hasDownload = true;
			$this->assign('pdfFile', $url);
		}
		
		$epubFile = $this->fileHandler->getStoryExtraFile($story->getAuthorName(), $story->title, $story->randomNum, '.epub');
		if (file_exists($epubFile))
		{
			$url = $this->fileHandler->getStoryExtraFileUrl($story->getAuthorName(), $story->title, $story->randomNum, '.epub');
			$hasDownload = true;
			$this->assign('epubFile', $url);
		}
		
		$this->assign('hasDownload', $hasDownload);
		
		/* get the story txt file */
		$file = $this->fileHandler->getStoryFile($story->getAuthorName(), $story->title, $story->randomNum);
		
		$fileHtml = $this->fileHandler->getStoryHtmlFile($story->getAuthorName(), $story->title, $story->randomNum);
		
		if (file_exists($fileHtml))
		{
			/** Load the content of the story html file. */
			$fileContent = file_get_contents($fileHtml);
		}
		else
		{
			/** Load the content of the text file. */
			$fileContent = nl2br(htmlspecialchars(file_get_contents($file)));
		}
		
		/** Inject the content into the webpage */
		$this->assign('storyHtml', $fileContent);
		
		$cmtParams = new BASE_CommentsParams('stories', 'story_comments');
		$cmtParams->setEntityId($params['storyID']);
		$cmtParams->setOwnerId($contentOwner);
		$cmtParams->setDisplayType(BASE_CommentsParams::DISPLAY_TYPE_BOTTOM_FORM_WITH_FULL_LIST);
		
		$cmtParams->setWrapInBox(false);
		$cmtParams->setAddComment(true);
		
		$storyCmts = new BASE_CMP_Comments($cmtParams);
		$this->addComponent('comments', $storyCmts);
	}
	
	public function featureStory(array $params)
	{
		if (!isset($params['storyID']))
		{
			throw new Redirect404Exception();
		}
		
		$story = $this->service->getStory($params['storyID']);
		
		if (!$story)
		{
			throw new Redirect404Exception();
		}
		
		$modPermissions = OW::getUser()->isAuthorized('stories');
		
		if (!$modPermissions)
		{
			throw new AuthorizationException();
		}
		
		$this->redirect(OW::getRouter()->urlForRoute('stories-view', array('storyID' => $story->id)));
		
	}
	
	public function deleteStory(array $params)
	{
		if (!isset($params['storyID']))
		{
			throw new Redirect404Exception();
		}
		
		$story = $this->service->getStory($params['storyID']);
		
		if (!$story)
		{
			throw new Redirect404Exception();
		}
		
		$modPermissions = OW::getUser()->isAuthorized('stories');
		$contentOwner = (int) $story->owner;
		$userId = OW::getUser()->getId();
		$ownerMode = $contentOwner == $userId;
		
		if (!$ownerMode && !$modPermissions)
        {
            throw new AuthorizationException();
        }
		
		$form = new Form('delete_story_form');
		$form->setEnctype(Form::ENCTYPE_MULTYPART_FORMDATA);
		$this->addForm($form);
		
		$selectField = new Selectbox('confirm_del');
        $selectField->setLabel('Confirm Delete');

        $selectField->setOptions(array(
            "1" => "No - Leave it here",
            "2" => "Yes - Get rid of it"
        ));
        
        $form->addElement($selectField);
		
		$submit = new Submit('submitStoryEdit');
		$submit->setValue('Confirm');
		$form->addElement($submit);
		
		/* Set the page title. */
		$this->setPageTitle('Delete Story');
		/* Set the page heading. */
		$this->setPageHeading('Are you sure you want to delete this Story?');
		$this->assign('storyTitle', $story->title);
		
		if (OW::getRequest()->isPost() && $form->isValid($_POST))
		{
			
			$data = $form->getValues();
			
			$res = $data['confirm_del'];
			
			if ($res == '2')
			{
				$this->service->deleteStory($story->id);
				
				OW::getFeedback()->info('Story Deleted');
				$this->redirect(OW::getRouter()->urlForRoute('stories-viewlist', array('listType' => 'latest')));
			}
			else
			{
				OW::getFeedback()->info('Story Not Deleted');
				$this->redirect(OW::getRouter()->urlForRoute('stories-view', array('storyID' => $story->id)));
			}
		}		
	}
	
	/** Utility function to load text from the language */
	private function text($prefix, $key, array $vars = null)
	{
		return OW::getLanguage()->text($prefix, $key, $vars);
	}
}