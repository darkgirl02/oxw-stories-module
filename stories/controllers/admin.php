<?php

class STORIES_CTRL_Admin extends ADMIN_CTRL_Abstract
{
	
	public function category()
	{
		$this->setPageTitle(OW::getLanguage()->text('stories', 'admin_cats_title'));
		
		$categoryNames = array();
		$deleteUrls = array();
		$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
		foreach ($categories as $category)
		{
			$categoryNames[$category->id]['name'] = $category->id;
			$categoryNames[$category->id]['categoryName'] = $category->name;
			$categoryNames[$category->id]['scode'] = $category->scode;
			$categoryNames[$category->id]['description'] = $category->description;
			$deleteUrls[$category->id] = OW::getRouter()->urlFor(__CLASS__, 'delete', array('id' => $category->id));
		}
		
		$this->assign('categories', $categoryNames);
		$this->assign('deleteUrls', $deleteUrls);
		
		$form = new Form('add_category');
		$this->addForm($form);
		
		$fieldShortCode = new TextField('shortCode');
		$fieldShortCode->setRequired();
		$fieldShortCode->setInvitation('Code');
		$fieldShortCode->setHasInvitation(true);
		$form->addElement($fieldShortCode);
		
		
		$fieldCategory = new TextField('category');
		$fieldCategory->setRequired();
		$fieldCategory->setInvitation('Label');
		$fieldCategory->setHasInvitation(true);
		$form->addElement($fieldCategory);
		
		$fieldDesc = new TextField('description');
		$fieldDesc->setRequired();
		$fieldDesc->setInvitation('Description');
		$fieldDesc->setHasInvitation(true);
		$form->addElement($fieldDesc);
		
		$submit = new Submit('submit');
		$submit->setValue(OW::getLanguage()->text('stories', 'form_add_category_submit'));
		$form->addElement($submit);
		
		if (OW::getRequest()->isPost())
		{
			if ($form->isValid($_POST))
			{
				$data = $form->getValues();
				STORIES_BOL_Service::getInstance()->addCategory($data['shortCode'], $data['category']);
				$this->redirect();
			}
		}
	}
	
	public function delete($params)
	{
		if (isset($params['id']))
		{
			STORIES_BOL_Service::getInstance()->deleteCategory((int) $params['id']);
		}
		$this->redirect(OW::getRouter()->urlForRoute('stories-admin'));
	}
	
}