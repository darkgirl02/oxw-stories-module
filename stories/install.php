<?php

OW::getPluginManager()->addPluginSettingsRouteName('stories', 'stories-admin');

BOL_LanguageService::getInstance()->addPrefix('stories', 'Stories');

$path = OW::getPluginManager()->getPlugin('stories')->getRootDir() . 'lang.zip';
OW::getLanguage()->importPluginLangs($path, 'stories');

/* Create the categories table */
$sql = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "stories_category` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
	`scode` VARCHAR(200) NOT NULL,
	`description` VARCHAR(200),
    PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql);

/* Create the stories table */
$sql2 = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "stories_story` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
	`owner` INT(11) NOT NULL,
    `title` VARCHAR(200) NOT NULL,
	`description` text NOT NULL,
	`author` INT(11) NOT NULL,
	`publicationDate` int(10) DEFAULT NULL,
	`tags` VARCHAR(200) NOT NULL,
	`randomNum` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql2);

/* Create the author table */
$sql3 = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "stories_author` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql3);

$sql4 = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "stories_story_featured` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
	`storyId` INT(200) NOT NULL default '0',
    PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql4);

$authorization = OW::getAuthorization();
$groupName = 'stories';
$authorization->addGroup($groupName);
$authorization->addAction($groupName, 'add');
$authorization->addAction($groupName, 'view', true);
$authorization->addAction($groupName, 'add_comment');
$authorization->addAction($groupName, 'modify');