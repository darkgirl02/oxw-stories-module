<?php

class STORIES_CLASS_NewFileHandler
{
	
	private static $classInstance;
	
	 
	public static function getInstance()
    {
        if ( null === self::$classInstance )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
	
	public function getStoryHtmlFile($author, $storyTitle, $randomNum)
	{
		$cleanName = $this->cleanText($author);
		$cleanTitle = $this->cleanText($storyTitle);
		return $this->getStoryDir($cleanName, $cleanTitle, $randomNum) . DIRECTORY_SEPARATOR . $cleanTitle . '.s';
	}
	
	public function getStoryFile($author, $storyTitle, $randomNum)
	{
		$cleanName = $this->cleanText($author);
		$cleanTitle = $this->cleanText($storyTitle);
		return $this->getStoryDir($cleanName, $cleanTitle, $randomNum) . DIRECTORY_SEPARATOR . $cleanTitle . '.txt';
	}
	
	public function getStoryExtraFile($author, $storyTitle, $randomNum, $format)
	{
		$cleanName = $this->cleanText($author);
		$cleanTitle = $this->cleanText($storyTitle);
		return $this->getExtraFilesDir($cleanName, $cleanTitle, $randomNum) . DIRECTORY_SEPARATOR . $cleanTitle . $format;
	}
	
	public function getStoryExtraFileUrl($author, $storyTitle, $randomNum, $file)
	{
		$cleanName = $this->cleanText($author);
		$cleanTitle = $this->cleanText($storyTitle);
		return OW::getPluginManager()->getPlugin('stories')->getUserFilesUrl() . $cleanName . DIRECTORY_SEPARATOR .
		$cleanTitle . '_' . $randomNum . DIRECTORY_SEPARATOR .
		'additional_files' . DIRECTORY_SEPARATOR .
		$cleanTitle . basename($file);
	}
	
	private function getExtraFilesDir($author, $title, $randomNum)
	{
		$dir = $this->getStoryDir($author, $title, $randomNum) . DIRECTORY_SEPARATOR . 'additional_files';
		if (!file_exists($dir))
		{
			mkdir($dir);
		}
		return $dir;
	}
	
	private function getStoryDir($author, $title, $randomNum)
	{
		$dir = $this->getAuthorDir($author) . DIRECTORY_SEPARATOR . $title . '_' . $randomNum;
		if (!file_exists($dir))
		{
			mkdir($dir);
		}
		return $dir;
	}
	
	private function getAuthorDir($author)
	{
		$dir = $this->getStoriesDir() . DIRECTORY_SEPARATOR . $author;
		if (!file_exists($dir))
		{
			mkdir($dir);
		}
		return $dir;
	}
	
	private function getStoriesDir()
	{
		return OW::getPluginManager()->getPlugin('stories')->getUserFilesDir();
	}
	
	/** Remove all whitespace and line-break characters from the input string */
	private function cleanText($input)
	{
		$pattern = '/\s*/m';
		$replace = '';
		$clean = preg_replace($pattern, $replace, $input);
		return $clean;
	}
	
}