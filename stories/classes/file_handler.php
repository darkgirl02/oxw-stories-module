<?php

class STORIES_CLASS_FileHandler
{
	
	private static $classInstance;
	
	 public static function getInstance()
    {
        if ( null === self::$classInstance )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
	
	/** Get the story.s file from the given story directory */
	public function getStoryFile($parent, $storyTitle)
	{
		$cleanName = $this->cleanText($storyTitle);
		return $parent . DIRECTORY_SEPARATOR . $cleanName . '.txt';
	}
	
	public function getAdditionalFileUrl($authorDir, $storyDir, $file)
	{
		return OW::getPluginManager()->getPlugin('stories')->getUserFilesUrl() . 
		basename($authorDir) . 
		DIRECTORY_SEPARATOR . 
		basename($storyDir). 
		DIRECTORY_SEPARATOR . 
		'additional_files' . 
		DIRECTORY_SEPARATOR . 
		basename($file);
	}
	
	public function getAdditionalFile($parent, $storyTitle, $format)
	{
		$cleanName = $this->cleanText($storyTitle);
		return $parent . DIRECTORY_SEPARATOR . $cleanName . $format;
	}
	
	/** Get the additional files directory for the given story directory. */
	public function getAdditionalFilesDir($parent)
	{
		$dir = $parent . DIRECTORY_SEPARATOR . 'additional_files';
		if (!file_exists($dir))
		{
			mkdir($dir);
		}
		return $dir;
	}
	
	/** Get the directory for the given story. */
	public function getStoryDir($parent, $storyTitle, $randomNum)
	{
		$cleanName = $this->cleanText($storyTitle);
		$dir = $parent . DIRECTORY_SEPARATOR . $cleanName . '_' . $randomNum;
		if (!file_exists($dir))
		{
			mkdir($dir);
		}		
		return $dir;
	}
	
	/** Get the directory for the given author */
	public function getAuthorDir($parent, $author)
	{
		$cleanName = $this->cleanText($author);
		$dir = $parent . DIRECTORY_SEPARATOR . $cleanName;
		if (!file_exists($dir))
		{
			mkdir($dir);
		}
		return $dir;
	}
	
	/** Get the 'stories' directory in ow_userfiles */
	public function getStoriesDir()
	{
		$userFiles = OW::getPluginManager()->getPlugin('stories')->getUserFilesDir();
		return $userFiles;
	}
	
	/** Remove all whitespace and line-break characters from the input string */
	public function cleanText($input)
	{
		$pattern = '/\s*/m';
		$replace = '';
		$clean = preg_replace($pattern, $replace, $input);
		return $clean;
	}
	
}