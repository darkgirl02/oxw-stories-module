<?php

class STORIES_CLASS_EventHandler
{
	
	private static $classInstance;
	
	const EVENT_STORY_ADD = 'stories.add_story';
	
	public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
	
	private function __construct() { }
	
	public function addStory(OW_Event $e)
	{
		$params = $e->getParams();
		
		if ( empty($params['userId']) || empty($params['title']) || empty($params['storyId']) )
        {
			return;
        }
        else
		{
			$service = STORIES_BOL_Service::getInstance();
			
			$storyEmbed = '<a href="' . '/stories/view/' . $params['storyId'] . '">' . $params['title'] . '</a>';
			$string = array(
				'key' => 'stories+feed_activity_posted_story_string',
				'vars' => array('story' => $storyEmbed)
			);
			
			OW::getEventManager()->trigger(new OW_Event('feed.action', array(
				'activityType' => 'comment',
				'pluginKey' => 'stories',
				'entityType' => 'story_comments',
				'entityId' => $params['storyId'],
				'userId' => $params['userId']
			), array(
				'string' => $string
			)));
		}
	}
	
	public function feedStoryComment(OW_Event $event)
	{
		$params = $event->getParams();
		
		if ($params['entityType'] != 'story_comments')
		{
			return;
		}
		
		$service = STORIES_BOL_Service::getInstance();
		$userId = $service->findStoryOwner($params['entityId']);
		
		$story = $service->getStory($params['entityId']);
		
		if ($userId == $params['userId'])
		{
			$storyEmbed = '<a href="' . '/stories/view/' . $story->id . '">' . $story->title . '</a>';
			$string = array(
				'key' => 'stories+feed_activity_owner_story_string',
				'vars' => array('story' => $storyEmbed)
			);
		}
		else
		{
			$userName = BOL_UserService::getInstance()->getDisplayName($userId);
			$userUrl = BOL_UserService::getInstance()->getUserUrl($userId);
			$userEmbed = '<a href="' . $userUrl . '">' . $userName . '</a>';
			$storyEmbed = '<a href="' . '/stories/view/' . $story->id . '">' . $story->title . '</a>';
			$string = array(
				'key' => 'stories+feed_activity_story_string',
				'vars' => array('user' => $userEmbed, 'story' => $storyEmbed)
			);
		}
		
		OW::getEventManager()->trigger(new OW_Event('feed.activity', array(
            'activityType' => 'comment',
            'activityId' => $params['commentId'],
            'entityId' => $params['entityId'],
            'entityType' => $params['entityType'],
            'userId' => $params['userId'],
            'pluginKey' => 'stories'
			
        ), array(
            'string' => $string
        )));
	}
	
	public function init()
	{
		$em = OW::getEventManager();
		
		$em->bind(self::EVENT_STORY_ADD, array($this, 'addStory'));
		$em->bind('feed.after_comment_add', array($this, 'feedStoryComment'));
	}
	
}