<?php

class STORIES_BOL_Service
{
	
	private static $classInstance;
	
	public static function getInstance()
	{
		if (null == self::$classInstance)
		{
			self::$classInstance = new self();
		}
		return self::$classInstance;
	}
	
	private function __construct()
	{
		
	}
	
	public function addCategory($shortCode, $name)
	{
		$category = new STORIES_BOL_StoryCategory();
		$category->scode = $shortCode;
		$category->name = $name;
		STORIES_BOL_StoryCategoryDao::getInstance()->save($category);
	}
	
	public function getCategoryList()
	{
		return STORIES_BOL_StoryCategoryDao::getInstance()->findAll();
	}
	
	public function deleteCategory($id)
	{
		$id = (int) $id;
		if ($id > 0)
		{
			STORIES_BOL_StoryCategoryDao::getInstance()->deleteById($id);
		}
	}
	
	public function addAuthor($name)
	{
		$author = new STORIES_BOL_StoryAuthor();
		$author->name = $name;
		STORIES_BOL_StoryAuthorDao::getInstance()->save($author);
		return $author->id;
	}
	
	public function getAuthorList()
	{
		return STORIES_BOL_StoryAuthorDao::getInstance()->findAll();
	}
	
	public function containsAuthor($name)
	{
		$authors = $this->getAuthorList();
		foreach ($authors as $author)
		{
			if ($author->name == $name)
			{
				return true;
			}
		}
		return false;
	}
	
	public function getAuthorById($id)
	{
		return STORIES_BOL_StoryAuthorDao::getInstance()->findById($id);
	}
	
	public function findAuthorIdByName($name)
	{
		$authors = $this->getAuthorList();
		foreach ($authors as $author)
		{
			if ($author->name = $name)
			{
				return $author->id;
			}
		}
		return false;
	}
	
	public function addStory($title, $description, $tags, $author, $owner, $date, $randomNum)
	{
		$story = new STORIES_BOL_Story();
		$story->title = $title;
		$story->description = $description;
		$story->setTags($tags);
		
		if ($this->containsAuthor($author))
		{
			$authorId = $this->findAuthorIdByName($author);
		} 
		else
		{
			$authorId = $this->addAuthor($author);
		}
		
		$story->author = $authorId;
		$story->owner = $owner;
		$story->publicationDate = $date;
		$story->randomNum = $randomNum;
		STORIES_BOL_StoryDao::getInstance()->save($story);
		return $story->id;
	}
	
	public function updateStory($story)
	{
		STORIES_BOL_StoryDao::getInstance()->save($story);
		
	}
	
	public function getStory($id)
	{
		return STORIES_BOL_StoryDao::getInstance()->findById($id);
	}
	
	public function getStoriesByAuthor($author)
	{
		$all = STORIES_BOL_StoryDao::getInstance()->getAllStories();
		
		$stories = array();
		
		foreach ($all as $story)
		{
			if ($story->author == $author)
			{
				$stories[$story->id] = $story;
			}
		}
		
		return $stories;
	}
	
	public function getStoriesByTag($tags)
	{
		$all = STORIES_BOL_StoryDao::getInstance()->getAllStories();
		
		$stories = array();
		
		foreach ($all as $story)
		{
			$cats = $story->getTags();
			foreach ($cats as $cat)
			{
				if ($cat == $tags)
				{
					$stories[$story->id] = $story;
				}					
			}
		}
		return $stories;
		
	}
	
	public function getAllStories()
	{
		return STORIES_BOL_StoryDao::getInstance()->findAll();
	}
	
	public function deleteStory($id)
	{
		$id = (int) $id;
		if ($id > 0)
		{
			STORIES_BOL_StoryDao::getInstance()->deleteById($id);
		}
	}
	
	public function findStoryOwner($storyId)
	{
		$story = $this->getStory($storyId);
		return $story->owner;
	}
	
}