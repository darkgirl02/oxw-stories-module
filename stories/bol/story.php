<?php

class STORIES_BOL_Story extends OW_Entity
{

	/** Title of the story */
	public $title;
	/** Description of the story */
	public $description;
	/** The authors name. */
	public $author;
	
	public $owner;
	
	public $tags;
	
	/** The publication date of the story */
	public $publicationDate;
	
	public $randomNum;
	
	public function setTags($tags)
	{
		$this->tags = json_encode($tags);
	}
	
	public function getTags()
	{
		if (empty($this->tags))
		{
			return array();
		}
		return json_decode($this->tags);
	}
	
	public function getAuthorName()
	{
		$aut = STORIES_BOL_Service::getInstance()->getAuthorById($this->author);
		return $aut->name; 
		
	}
	
	public function getAuthor()
	{
		return STORIES_BOL_Service::getInstance()->getAuthorById($this->author);
	}
	
	public function getOwnerName()
	{
		return BOL_UserService::getInstance()->getDisplayName($this->owner);
	}
	
}