<?php

class STORIES_BOL_StoryFeaturedDao extends OW_BaseDao
{
	protected function __construct()
	{
		parent::__construct();
	}
	
	private static $classInstance;
	
	public static function getInstance()
	{
		if (self::$classInstance === null)
		{
			self::$classInstance = new Self();
		}
		return self::$classInstance;
	}
	
	public function getDtoClassName()
	{
		return 'STORIES_BOL_StoryFeatured';
	}
	
	public function getTableName()
	{
		return OW_DB_PREFIX . 'stories_story_featured';
	}
	
	
	
}