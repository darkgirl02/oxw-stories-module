<?php

class STORIES_BOL_StoryCategoryDao extends OW_BaseDao
{
	protected function __construct()
	{
		parent::__construct();
	}
	
	private static $classInstance;
	
	public static function getInstance()
	{
		if (self::$classInstance === null)
		{
			self::$classInstance = new Self();
		}
		return self::$classInstance;
	}
	
	public function getDtoClassName()
	{
		return 'STORIES_BOL_StoryCategory';
	}
	
	public function getTableName()
	{
		return OW_DB_PREFIX . 'stories_category';
	}
	
}