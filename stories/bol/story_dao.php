<?php

class STORIES_BOL_StoryDao extends OW_BaseDao
{
	protected function __construct()
	{
		parent::__construct();
	}
	
	private static $classInstance;
	
	const CACHE_TAG_STORY_LIST = 'story.list';
	
	public static function getInstance()
	{
		if (self::$classInstance === null)
		{
			self::$classInstance = new Self();
		}
		return self::$classInstance;
	}
	
	public function getDtoClassName()
	{
		return 'STORIES_BOL_Story';
	}
	
	public function getTableName()
	{
		return OW_DB_PREFIX . 'stories_story';
	}
	
	public function getFeaturedStories()
	{
		$allIds = STORIES_BOL_StoryFeaturedDao::getInstance()->findAll();
		$ids = array();
		foreach ($allIds as $id)
		{
			$ids[$id->id] = $id->storyId;
		}
		return $this->findByIdList($ids);
	}
	
	public function getLatestStories($limit)
	{
		$first = 0;
		$cacheLifeTime = $first == 0 ? 24 * 3600 : null;
        $cacheTags = $first == 0 ? array(self::CACHE_TAG_STORY_LIST) : null;
		
		$query = "
			SELECT
				`c`.*
			FROM
				`" . $this->getTableName() . "` AS `c`
			ORDER BY
				`c`.`publicationDate` DESC
			LIMIT
				:first, :limit";

		$qParams = array('first' => (int) $first, 'limit' => (int) $limit);
		return $this->dbo->queryForObjectList($query, 'STORIES_BOL_Story', $qParams, $cacheLifeTime, $cacheTags);
	}
	
	public function getAllStories()
	{
		$first = 0;
		$cacheLifeTime = $first == 0 ? 24 * 3600 : null;
        $cacheTags = $first == 0 ? array(self::CACHE_TAG_STORY_LIST) : null;
		
		$query = "
			SELECT
				`c`.*
			FROM
				`" . $this->getTableName() . "` AS `c`
			ORDER BY
				`c`.`publicationDate` DESC
			";

		$qParams = array();
		return $this->dbo->queryForObjectList($query, 'STORIES_BOL_Story', $qParams, $cacheLifeTime, $cacheTags);
	}
	
}