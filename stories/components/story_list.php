<?php

class STORIES_CMP_StoryList extends OW_Component
{
	private $storyService;
	
	 public function __construct( array $params )
    {
        parent::__construct();
		
		$this->storyService = STORIES_BOL_Service::getInstance();
		
		
		$listType = isset($params['type']) ? $params['type'] : '';
		$tag = isset($params['tag']) ? $params['tag'] : '';
		$authorId = isset($params['author']) ? $params['author'] : '';
		
		
		
		$stories = array();
		$storyCategories = array();
		$categories = STORIES_BOL_Service::getInstance()->getCategoryList();
		foreach ($categories as $category)
		{
			$storyCategories[$category->scode]['scode'] = $category->scode;
			$storyCategories[$category->scode]['name'] = $category->name;
		}
		
		$startTime = $this->millitime();
		
		if ($authorId)
		{
			$storiesD = $this->storyService->getStoriesByAuthor($authorId);
		}
		else if (strlen($tag))
		{
			$storiesD = $this->storyService->getStoriesByTag($tag);
		}
		else
		{
			if ($listType == 'all')
			{
				//$storiesD = $this->storyService->getAllStories();
				$storiesD = STORIES_BOL_StoryDao::getInstance()->getAllStories();
			}
			else if ($listType == 'featured')
			{
				$storiesD = STORIES_BOL_StoryDao::getInstance()->getFeaturedStories();
			}
			else if ($listType == 'toprated')
			{
				$storiesD = false;
			}
			else if ($listType == 'latest')
			{
				$storiesD = STORIES_BOL_StoryDao::getInstance()->getLatestStories(25);
			}
			else
			{
				$storiesD = false;
			}
		}
		
		if ($storiesD)
		{
			foreach($storiesD as $story)
			{
				$stories[$story->id]['id'] = $story->id;
				$stories[$story->id]['title'] = $story->title;
				$stories[$story->id]['authorName'] = $story->getAuthorName();
				$stories[$story->id]['authorId'] = $story->getAuthor()->id;
				$stories[$story->id]['description'] = $story->description;
				$stories[$story->id]['publicationDate'] = $story->publicationDate;
			
				$tags = array();
				foreach ($story->getTags() as $tag)
				{
					$tags[$tag] = $storyCategories[$tag];
				}
				$stories[$story->id]['tags'] = $tags;
			}
		}
		
		$endTime = $this->millitime();
		
		if ($stories)
		{
			$this->assign('no_content', null);
			$this->assign('stories', $stories);
			$this->assign('storyCount', sizeof($stories, 0));
			$this->assign('loadTime', ($endTime - $startTime));
		}
		else
		{
			$this->assign('no_content', 'No stories found :(');
		}
		
	}
	
	function millitime()
	{
		$microtime = microtime();
		$comps = explode(' ', $microtime);

		// Note: Using a string here to prevent loss of precision
		// in case of "overflow" (PHP converts it to a double)
		return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
	}
}
